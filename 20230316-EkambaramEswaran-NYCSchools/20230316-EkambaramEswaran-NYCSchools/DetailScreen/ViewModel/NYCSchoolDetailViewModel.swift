//
//  NYCSchoolDetailViewModel.swift
//  20230316-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 3/16/23.
//

import Foundation

//MARK: ViewModel class for Detail screen
class NYCSchoolDetailViewModel: NSObject {
    
    private var apiService : NetworkManagerProtocol?
    var schoolId: String?
    
    private(set) var schoolDetails : [NYCSchoolDetailModel]? {
        didSet {
            self.bindNYCSchoolDetailViewModelToController()
        }
    }
    
    var bindNYCSchoolDetailViewModelToController : (() -> ()) = {}
    
    init(schoolId: String, apiService: NetworkManagerProtocol = NetworkManager()) {
        super.init()
        self.schoolId = schoolId
        self.apiService = apiService
    }
    
    func callFuncToGetSchoolDetailData() -> () {
        
        if let url = URL(string: NYCSchoolAPIEndpoints.schoolDetailsAPI + (schoolId ?? "")) {
            self.apiService?.fetchAPIService(url, model: [NYCSchoolDetailModel].self, completion: { [weak self] result in
                switch result {
                case .success(let response):
                    self?.schoolDetails = response
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
    }
}
