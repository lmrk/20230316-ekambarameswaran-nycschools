//
//  Constant.swift
//  20230316-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 3/16/23.
//

import Foundation

//MARK: List of API service calls
struct NYCSchoolAPIEndpoints {
    static let schoolsAPI = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schoolDetailsAPI = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}

